# Agenda
- Who?
- What?
- How?

---

# Who?

Note:
nitin arora
- B.Tech; Galgotia College of Engineering & Technology; 2004 - 2008
- MBA; IBS, Hyderabad; 2010 - 2012
- Masters, Business Analytics & Data Science; University of Texas, Dallas; 2016 - 2017
- Data Science Intern; AT&T; Sep 17 - Dec 17
- Data Scientist; Verizon; Feb 18 - present

? Questions :
- Who are you?
- Are you sure about that?
- Are you sitting with your friend?
- Are you sure about that?
- Are you sure who the person sitting next to you are?

---

# What?

+++

## Identity

Note:
- what creeps you out?
  - Rahul / Arun / Aishwarya / Lakshmi / Aparna

+++

## Privacy
@css[ fragment ]( social media )

Note:
- what is privacy?
  - not giving data to corporations without your consent
  - not letting corporations use your data to sell YOU
- have nothing to hide?
  - password to Google; why not?
  - Bank card / ATM PIN; Netbanking username / password
- social media
  - anyone without it? congratulations, you don't exist
  - social acceptance / acknowledgement

---

# How?
@ul
- online
- mobile
@ulend

Note:
- online
  - ever read ToS / Privacy Policies?
  - EULA? ever read EULA?
- mobile
  - ever audited the permissions given to mobile apps?
  - walled gardens; web-apps packaged into apps with unnecessary permissions - collection of user data
  - want to use service, accept permissions
  - third-party apps on social media
- sources / links
  - https://arstechnica.com/information-technology/2018/03/facebook-scraped-call-text-message-data-for-years-from-android-phones/
  - https://factordaily.com/zapr-acr-audio-fingerprinting-technology-in-mobile-apps/
  - https://beebom.com/hotstar-newsdog-apps-spying-phone-mic/

+++

@img[ fragment zoom-in span-45 north-west ](https://upload.wikimedia.org/wikipedia/commons/9/96/Microsoft_logo_%282012%29.svg)
@img[ fragment zoom-in span-15 north-east ](https://upload.wikimedia.org/wikipedia/commons/3/31/Apple_logo_white.svg)
@img[ fragment zoom-in span-30 west ](https://upload.wikimedia.org/wikipedia/commons/7/7a/Alphabet_Inc_Logo_2015.svg)
@img[ fragment zoom-in span-30 center ](https://upload.wikimedia.org/wikipedia/commons/8/87/Facebook_Logo_%282015%29_light.svg)
@img[ fragment zoom-in span-30 east ](https://upload.wikimedia.org/wikipedia/commons/7/70/Amazon_logo_plain.svg)

@snap[ south span-100 ]
## the 5
@size[ medium ]( Source : https://www.bloomberg.com/opinion/articles/2017-11-15/the-big-five-could-destroy-the-tech-ecosystem <br> )
@size[ medium ]( All brand logos are trademarks owned by the respective companies; sourced from Wikimedia Commons )
@snapend

+++

## Myth-busting

@snap[ fragment ]
### Nothing is free
as in free food
@snapend
####  @css[ fragment ]( You pay with )
## @css[ fragment ]( yourself )
@css[ fragment ]( your persona, ) @css[ fragment ]( personality ) @css[ fragment ]( and character )

Note:
- Gmail
  - ads from emails
- e-commerce
  - ads from websites on email and social media
- Cambridge Analytica Scandal
  - third-party app harvesting data about users without consent.
  - published to internet
  - used by political campaigns - Trump, Ted Cruz, Mexican General Election 2018
  - source : https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal
- mobile data scraping (facebook) : https://arstechnica.com/information-technology/2018/03/facebook-scraped-call-text-message-data-for-years-from-android-phones/
- audio content recognition (ACR)
  - listen-in on ambient noises around mobile device to determine data
  - fingerprints content on both ends (client and server) and pattern matches
  - patented by Bengaluru-based Zapr
  - apps with technology includes Hotstar, Chhotta Bheem games, Real Cricket 17, Teen Patti, Ultimate Rummy Circle, etc.
  - sources / links
    - https://factordaily.com/zapr-acr-audio-fingerprinting-technology-in-mobile-apps/
    - https://beebom.com/hotstar-newsdog-apps-spying-phone-mic/
- Licensing of data / media uploaded : https://www.plagiarismtoday.com/2015/05/13/does-facebook-really-own-your-photos/
- WhatsApp's End-to-End encryption is broken
  - source links
    - https://www.heise.de/ct/artikel/Keeping-Tabs-on-WhatsApp-s-Encryption-2630361.html
    - https://thehackernews.com/2017/01/whatsapp-encryption-backdoor.html

---

# Trust

+++

## Who?
@css[ fragment ]( NO ONE )

+++

## What?
@css[ fragment ]( nothing )
<br>
@css[ fragment ]( no, really... )
@css[ fragment ]( ABSOLUTELY NOTHING )
@css[ fragment ]( unless ... )
<br>
### @css[ fragment ]( open-source )

+++

## How?

@ul
- control
- own
@ulend

### @css[ fragment ]( YOUR DATA )

+++

## Practical?
### @css[ fragment ]( Alternatives )

Note:
- Trust open-source
  - audit code yourself
  - vetted / audited by community / professionals
- self-host
- privacytools.io
  - email -> no real FLOSS alternative to email other than self-hosting which is complicated
  - YouTube -> PeerTube / WebTorrent
  - Facebook -> Diaspora
  - Twitter -> Mastodon / GNU Social / pump.io
  - WhatsApp -> Telegram / Signal
- friends / contacts is not on X

---

the most important question
## @css[ fragment ]( Why? )
